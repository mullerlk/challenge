<?php
  include 'connect.php';
  if (isset($_POST['submit'])) {
    $product=$_POST['product'];
    $sku=$_POST['sku'];
    $price=$_POST['price'];
    $description=$_POST['description'];
    $quantity=$_POST['quantity'];
    $category=$_POST['category'];
    $sql="INSERT INTO `crud`(`product`, `sku`, `price`, `description`, `quantity`, `category`) VALUES ('$product','$sku','$price','$description','$quantity','$category')";
    $result=mysqli_query($con,$sql);
    if(!$result){
      echo "Insert error.";
      exit;
    }
    header('location:display.php');
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet">

    <title>WEBJump Challenge</title>
  </head>
  <body>
    <div class="container my-5">
      <form method="POST">
        <div class="mb-3">
          <label>Product</label>
          <input type="text" class="form-control" placeholder="Enter the product's name" name="product" value=<?php echo $product; ?>>
        </div>
        <div class="mb-3">
          <label>SKU</label>
          <input type="number" class="form-control" placeholder="Enter SKU" name="sku" value=<?php echo $sku; ?>>
        </div>
        <div class="mb-3">
          <label>Price</label>
          <input type="text" class="form-control" placeholder="Enter $ price" name="price" value=<?php echo $price; ?>>
        </div>
        <div class="mb-3">
          <label>Description</label>
          <input type="text" class="form-control" placeholder="Product's description" name="description" value=<?php echo $description; ?>>
        </div>
        <div class="mb-3">
          <label>Quantity</label>
          <input type="number" class="form-control" placeholder="Quantity" name="quantity" value=<?php echo $quantity; ?>>
        </div>
        <div class="mb-3">
          <label>Category</label>
          <input type="text" class="form-control" placeholder="Category" name="category" value=<?php echo $category; ?>>
        </div>
        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
      </form>
    </div>
  </body>
</html>
