Como rodar o programa:
  - adicione o diretório 'challenge' em seu diretório root de WEB Service (geralmente, em linux, /srv/http/);
  - crie um banco de dados (utilizei phpMyAdmin) com o nome 'challenge' e uma tabela com o nome 'crud' contendo
    as seguintes entradas: id(INT)(AI and primary key), product(VARCHAR), sku(INT), price(VARCHAR), description(VARCHAR), quantity(INT), category(VARCHAR);
  - no browser: http://localhost/challenge/user.php

Apenas uma versão criada que nào corresponde ao produto final.
MOTIVO: primeiro programa em php+sql+html.
DIFICULDADES: como linkar diferentes formas de tables a database.

tutoriais assistidos para aprender php e sql:
- PHP: https://www.youtube.com/watch?v=OK_JCtrrv-c
- SQL: https://www.youtube.com/watch?v=HXV3zeQKqGY