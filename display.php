<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet">
    <title>Challenge WEBJump! - Crud Operations</title>
  </head>
  <body>
    <div class="container">
      <button class="btn btn-primary my-5" type="button" name="button">
        <a href="user.php" class="text-light">Add user</a>
      </button>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Product</th>
            <th scope="col">SKU</th>
            <th scope="col">Price</th>
            <th scope="col">Description</th>
            <th scope="col">Quantity</th>
            <th scope="col">Category</th>
            <th scope="col">Operations</th>
          </tr>
        </thead>
        <tbody>
          <?php
            include 'connect.php';
            $sql="SELECT * FROM `crud`";
            $result=mysqli_query($con,$sql);
            if ($result) {
              while ($row=mysqli_fetch_assoc($result)) {
                $id=$row['id'];
                $product=$row['product'];
                $sku=$row['sku'];
                $price=$row['price'];
                $description=$row['description'];
                $quantity=$row['quantity'];
                $category=$row['category'];
                echo '<tr>
                  <th>'.$id.'</th>
                  <td>'.$product.'</td>
                  <td>'.$sku.'</td>
                  <td>'.$price.'</td>
                  <td>'.$description.'</td>
                  <td>'.$quantity.'</td>
                  <td>'.$category.'</td>
                  <td>
                    <button type="button" name="button" class="btn btn-primary">
                      <a href="update.php?updateid='.$id.'" class="text-light">Update</a>
                    </button>
                    <button type="button" name="button" class="btn btn-danger">
                      <a href="delete.php?deleteid='.$id.'" class="text-light">Delete</a>
                    </button>
                  </td>
                </tr>';
              }
            }
          ?>
        </tbody>
      </table>
    </div>
  </body>
</html>
